##1. Qué velocidades máximas se alcanzan con los estándares: 
##802.11a, 802.11b, 802.11g, 802.11n, 802.11ac, 802.11ad y en qué rango de frecuencias trabaja cada estándar.

A partir de la siguiente entrada de Wikipedia:

https://en.m.wikipedia.org/wiki/IEEE_802.11

Interpretar la tabla del apartado protocol

ESTANDAR          VELOCIDAD(Mbit/s)  								RANGO DE FRECUENCIA(GHz)

802.11a 	6, 9, 12, 18, 24, 36, 48, 54											5   3.7
802.11b 	1, 2, 5.5, 11																2.4
802.11g 	6, 9, 12, 18, 24, 36, 48, 54												2.4

802.11n		400 ns GI : 7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65, 72.2 [B]				2.4/5
			800 ns GI : 6.5, 13, 19.5, 26, 39, 52, 58.5, 65 [C] 


802.11ac 400 ns GI : 7.2, 14.4, 21.7, 28.9, 43.3, 57.8, 65, 72.2, 86.7, 96.3 [B] 		5
		 800 ns GI : 6.5, 13, 19.5, 26, 39, 52, 58.5, 65, 78, 86.7 [C]

		 400 ns GI : 15, 30, 45, 60, 90, 120, 135, 150, 180, 200 [B]
		 800 ns GI : 13.5, 27, 40.5, 54, 81, 108, 121.5, 135, 162, 180 [C]
		
		 400 ns GI : 32.5, 65, 97.5, 130, 195, 260, 292.5, 325, 390, 433.3 [B]
		 800 ns GI : 29.2, 58.5, 87.8, 117, 175.5, 234, 263.2, 292.5, 351, 390 [C]

		 400 ns GI : 65, 130, 195, 260, 390, 520, 585, 650, 780, 866.7 [B]
		 800 ns GI : 58.5, 117, 175.5, 234, 351, 468, 702, 780 [C]

802.11ad   Up to 6,912 (6.75 Gbit/s) [10]									60

##2. Qué estándar de seguridad se debe usar. Ordena de menor a mayor la seguridad de los

#estándares WEP64, WEP128, WPA1-PSK, WPA1-

Lectura de referencia: http://www.islabit.com/51272/seguridad-wi-fi-deberiamos-usar-wpa2-tkip-wpa2-aes-o-ambos.html


Abiertas (riesgo): Las redes Wi-Fi abiertas no tienen contraseña, por lo que queda claro que no se aconseja de ninguna forma.
WEP de 64 (riesgo): El viejo estándar de encriptación WEP es vulnerable y no se debe utilizar. Su nombre, que significa “Wired Equivalent Privacy”.
WEP de 128 (riesgo): WEP con un cifrado de mayor tamaño, pero igual inseguro.
WPA-PSK (TKIP): Este es básicamente el cifrado estándar WPA o WPA1. Se ha superado y no es seguro.
WPA-PSK (AES): Este elige el protocolo inalámbrico WPA con el cifrado más moderno AES. Los dispositivos que soportan AES casi siempre soportarán WPA2, mientras que los dispositivos que requieran WPA1 casi nunca admitirán el cifrado AES. Esta opción tiene muy poco sentido.
WPA2-PSK (TKIP): Se utiliza el estándar WPA2 con cifrado TKIP. Esta opción no es segura, sin embargo, es la mejor opción si se tienen dispositivos antiguos que no soportan una red WPA2-PSK (AES).
WPA2-PSK (AES): Esta es la opción más segura. Utiliza WPA2, el último estándar de encriptación Wi-Fi, y el más reciente protocolo de encriptación AES. Debes utilizar esta opción. En los routers con interfaces menos confusas, la opción marcada “WPA2” o “WPA2-PSK” probablemente solo utilice AES, ya que es una elección de sentido común.
WPAWPA2-PSK (TKIP / AES): Esto permite tanto WPA y WPA2 con TKIP y AES. Esto proporciona la máxima compatibilidad con todos los dispositivos antiguos, sin embargo, aunque sea una opción habitualmente predeterminada por los routers para evitar problemas con los dispositivos, es una opción poco aconsejable ya que termina siendo también vulnerable. Esta opción TKIP + AES también pueden ser llamada WPA2-PSK modo “mezcla”.





##3. Qué desventajas a parte de la seguridad tiene usar compatibilidad con TKIP?


##4. Si se realiza un ataque de fuerza bruta contra WPA2 PSK que recomendación de longitud 
##y combinaciones de tipos de carácteres recomendarías:

Lectura de referencia: https://www.acrylicwifi.com/blog/es-segura-red-wifi-wpa-wpa2/


##5. Cual es la función de un servidor radius



Una de las características más importantes del protocolo RADIUS es su capacidad de manejar sesiones, notificando cuándo comienza y termina una conexión, así que al usuario se le podrá determinar su consumo y facturar en consecuencia; los datos se pueden utilizar con propósitos estadísticos.

##6. Seguridad enterprise. Diferentes sistemas para autenticarse, ¿cual recomendarías?


Password-Based Authentication

The vast majority of authentication methods rely on username/password. 
It’s the easiest to deploy since most institutions already have some sort of credentials 
set up, but you’re still susceptible to all of the problems of passwords without an onboarding system (see below).

For password based authentication, there are basically 2 options: PEAP and EAP-TTLS. 
They both are functionally similar, but TTLS is not supported in any Microsoft OS before 
Windows 8 without using a third party supplicant like our Enterprise Client. At this point, 
most institutions have deployed or made the switch to PEAP. However, you can’t deploy PEAP 
without either using Active Directory (a proprietary Microsoft service) or leaving your passwords unencrypted.


Token-based Authentication

Tokens used to always be physical devices in the form of key fobs or dongles that would be distributed to users, 
generating numbers in sync with a server to add extra validation to a connection. But dongles are expensive and 
get out of sync with the servers from time to time, but at least you could carry them around. 
They also could have advanced features like fingerprint scanners or plug in with USB.

Physical tokens are still in use, but their popularity is waning as smartphones have made them redundant. 
What you used to have on a fob can now be put into an app. There are also many other ways to do two-factor 
authentication outside of the EAP method itself, like using text messages or emails to validate a device.


Certificate-Based Authentication

Certificates have long been a mainstay of authentication in general, 
but have not typically been deployed in BYOD settings since it requires 
getting users to install them on their own devices. 
However, once a certificate is installed, they are amazingly convenient.
They are not affected by password change policy, 
they are far safer than username/password, and devices authenticate faster.
 
 
 
 
 
 
 
